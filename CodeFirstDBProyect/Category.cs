﻿using System.ComponentModel.DataAnnotations.Schema;


namespace CodeFirstDBProyect
{
        [Table(" categories ")]
        public class Category
        {
                private readonly ObservableListtSource<Product> _products =
 new ObservableListtSource<Product>() ;

                 [Column(" category_id ")]
                 public int CategoryId { get; set; }
                 [Column(" name ")]
                 public string Name { get; set; }
                 public virtual ObservableListtSource< Product > Products { get {
                      return _products ; } }

        }
}
